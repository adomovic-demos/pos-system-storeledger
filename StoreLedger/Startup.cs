using System;
using System.IO;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using StoreLedger.Model;

namespace StoreLedger
{
    public partial class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => false;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            //cookie authentication
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(CookieAuthenticationDefaults.AuthenticationScheme, options =>
                {
                    options.AccessDeniedPath = new PathString("/accessdenied");
                    options.LoginPath = new PathString("/login");
                    options.SlidingExpiration = true;
                });


            services.AddDbContext<StoreData>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConn")));

            DependencyInjects(services);

            services.AddDistributedMemoryCache();
            services.AddSession(options =>
            {
                // Set a short timeout for easy testing.
                options.Cookie.HttpOnly = true;
                options.Cookie.Name = ".StoreLedger.Session";
                options.Cookie.SecurePolicy = CookieSecurePolicy.SameAsRequest;
                options.IdleTimeout = TimeSpan.FromMinutes(10);
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseDeveloperExceptionPage(); //app.UseExceptionHandler("/Error");
            }

            //run migration for hubtel portal utilities
            using (var scope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                scope.ServiceProvider.GetService<StoreData>().Database.Migrate();
            }


            app.UseStatusCodePages("text/html", "<h1>Error! Status Code {0}</h1>");
            app.UseSession();
            app.UseStaticFiles();
            //app.UseCookiePolicy();
            app.UseAuthentication();
            app.UseMvc();

           // Bootstrap();
        }

    //    public async void Bootstrap()
    //    {
    //        var options = new BrowserWindowOptions
    //        {
    //            WebPreferences = new WebPreferences
    //            {
    //                WebSecurity = false,
    //                NodeIntegration = false,
    //                AllowRunningInsecureContent = true
                    
    //            } ,
    //            AutoHideMenuBar = true,
    //            AlwaysOnTop = true
    //        };

    //        await Electron.WindowManager.CreateWindowAsync(options);
    //    }
    }
}
