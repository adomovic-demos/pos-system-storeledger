﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;

namespace StoreLedger.Pages.Inventory
{
    [Authorize]
    public class IndexModel : BasePageModel
    {
        private readonly UserRepo _userRepo;
        private readonly UserRoleRepo _userRoleRepo;
        private readonly UserActivity _userActivity;
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryCategoryRepo _categoryRepo;

        public IndexModel(UserRepo userRepo, UserRoleRepo userRoleRepo, UserActivity userActivity, 
            InventoryRepo inventoryRepo, InventoryCategoryRepo categoryRepo)
        {
            _userRepo = userRepo;
            _userRoleRepo = userRoleRepo;
            _userActivity = userActivity;
            _inventoryRepo = inventoryRepo;
            _categoryRepo = categoryRepo;
            Singles = new List<Model.InventoryView>();
            Packs = new List<Model.InventoryView>();
        }

        public string Message { get; set; }
        public SelectList Roles { get; set; }

        public List<Model.InventoryView> Singles { get; set; }
        public List<Model.InventoryView> Packs { get; set; }
        public SelectList CategoryList { get; set; }


        public async Task OnGetAsync(int? categoryId)
        {
            var items = await _inventoryRepo.GetWithStock();
            List<InventoryView> filteredItems;

            if (categoryId != null && Convert.ToInt32(categoryId) > 0)
                filteredItems = items.Where(c => c.InventoryCategoryId == categoryId).ToList();
            else
                filteredItems = items;

            Packs = filteredItems.Where(c => c.ItemType == Defaults.ItemTypeOption.Pack.ToString()).ToList();
            Singles = filteredItems.Where(c => c.ItemType == Defaults.ItemTypeOption.Single.ToString()).ToList();

            var cats = await _categoryRepo.GetHavningInventory();
            CategoryList = new SelectList(cats.AsEnumerable(), "InventoryCategoryId","Name", categoryId);
        }

       
    }
}
