﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StoreLedger.Activities;
using StoreLedger.Models;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages
{
    public class LoginModel : BasePageModel
    {
        private readonly LoginActivity _loginActivity;

        public LoginModel(LoginActivity loginActivity)
        {
            _loginActivity = loginActivity;
        }

        [BindProperty]
        public LoginViewModel Login { get; set; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnGetLogoutAsync()
        {
            HttpContext.Session.Clear();
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);

            return RedirectToPage("/login");
        }

        public async Task<IActionResult> OnPostLoginAsync()
        {
            var results = await _loginActivity.Login(HttpContext, Login.Username, Login.Password);

            if(!results.IsSuccessful)
                FlashError(results.Message);
            
            return RedirectToPage("/index");
        }
    }
}