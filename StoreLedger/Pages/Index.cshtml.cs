﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly TransactionRepo _transactionRepo;

        public DashboardViewModel Sales { get; set; }

        public IndexModel(TransactionRepo transactionRepo)
        {
            _transactionRepo = transactionRepo;

            Sales = new DashboardViewModel();
        }



        public async Task OnGetAsync()
        {
            var today = DateTime.Today;
            var todaySales = await _transactionRepo.GetTotal(today);
            Sales.Daily = new DayViewModel
            {
                Date = today,
                Total = todaySales.Total
            };

            var weekStart = Utils.GetFirstDayOfWeek(today);
            var weekEnd = Utils.GetLastDayOfWeek(today);
            var weekSales = await _transactionRepo.GetTotal(weekStart, weekEnd);
            Sales.Weekly = new DaysViewModel
            {
                From = weekStart,
                To = weekEnd,
                Total = weekSales.Total
            };

            var daysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
            var monthStart = new DateTime(today.Year, today.Month, 1);
            var monthEnd = monthStart.AddDays(daysInMonth-1);
            var monthSales = await _transactionRepo.GetTotal(monthStart, monthEnd);
            Sales.Montly = new DaysViewModel
            {
                From = monthStart,
                To = monthEnd,
                Total = monthSales.Total
            };

            
            var yearEnd = today;
            var yearStart = yearEnd.AddMonths(-12);
            var yearlySales = await _transactionRepo.GetTotal(yearStart, yearEnd);
            Sales.Yearly = new DaysViewModel
            {
                From = yearStart,
                To = yearEnd,
                Total = yearlySales.Total
            };

        }

        public async Task<JsonResult> OnGetTrendsAsync(string period)
        {
            DateTime from;
            DateTime to;
            var today = DateTime.Today;

            var trends = new List<TransactionTrend>();
            var monthlyTrends = new List<TransactionTrendMonthly>();
            switch (period.ToLower())
            {
                case "week":
                    @from = Utils.GetFirstDayOfWeek(today);
                    to = Utils.GetLastDayOfWeek(today);
                    trends = await _transactionRepo.GetTrends(@from, to);
                    break;
                case "month":
                    var daysInMonth = DateTime.DaysInMonth(today.Year, today.Month);
                    @from = new DateTime(today.Year, today.Month, 1);
                    to = @from.AddDays(daysInMonth - 1);
                    trends = await _transactionRepo.GetTrends(@from, to);
                    break;
                case "last3months":
                    var last3Months = today.AddMonths(-3);
                    from = new DateTime(last3Months.Year, last3Months.Month, 1);
                    to = today;
                    monthlyTrends = await _transactionRepo.GetTrendsMonthly(@from, to);
                    break;
                case "last6months":
                    var last6months = today.AddMonths(-6);
                    from = new DateTime(last6months.Year, last6months.Month, 1);
                    to = today;
                    monthlyTrends = await _transactionRepo.GetTrendsMonthly(@from, to);
                    break;
                case "last12months":
                    var last12Months = today.AddMonths(-12);
                    from = new DateTime(last12Months.Year, last12Months.Month, 1);
                    to = today;
                    monthlyTrends = await _transactionRepo.GetTrendsMonthly(@from, to);
                    break;
            }

            if (monthlyTrends.Any())
            {
                return new JsonResult(new
                {
                    Labels = monthlyTrends.Select(c =>  $"{c.MonthName} {c.Year}" ).ToArray(),
                    Series = monthlyTrends.Select(c => c.Total).ToArray()
                });
            }

            return new JsonResult(new
            {
                Labels = trends.Select(c => c.DateCreated.ToString("dd-MMM-yyyy")).ToArray(),
                Series = trends.Select(c => c.Total).ToArray()
            });
        }
    }
}
