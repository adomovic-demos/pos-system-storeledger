﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;

namespace StoreLedger.Pages.Admin.Inventory
{
    [Authorize]
    public class AdminInventoryPage : BasePageModel
    {
        private readonly UserRepo _userRepo;
        private readonly UserRoleRepo _userRoleRepo;
        private readonly UserActivity _userActivity;
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly InventoryActivity _inventoryActivity;
        private readonly InventoryStockRepo _inventoryStockRepo;

        public AdminInventoryPage(UserRepo userRepo, UserRoleRepo userRoleRepo, UserActivity userActivity, 
            InventoryRepo inventoryRepo, InventoryCategoryRepo categoryRepo, InventoryActivity inventoryActivity, 
            InventoryStockRepo inventoryStockRepo)
        {
            _userRepo = userRepo;
            _userRoleRepo = userRoleRepo;
            _userActivity = userActivity;
            _inventoryRepo = inventoryRepo;
            _categoryRepo = categoryRepo;
            _inventoryActivity = inventoryActivity;
            _inventoryStockRepo = inventoryStockRepo;
            Singles = new List<Model.Inventory>();
            Packs = new List<Model.Inventory>();
            InventoryStocks = new List<InventoryStock>();
        }

        public string Message { get; set; }
        public SelectList Roles { get; set; }

        public List<Model.Inventory> Singles { get; set; }
        public List<Model.Inventory> Packs { get; set; }
        public SelectList CategoryList { get; set; }
        public List<InventoryStock> InventoryStocks { get; set; }

        public async Task OnGetAsync(int? categoryId)
        {
            var items = await _inventoryRepo.GetAllAsync(c => c.Category);//  _inventoryRepo.GetWithStock();
            List<Model.Inventory> filteredItems;

            if (categoryId != null && Convert.ToInt32(categoryId) > 0)
                filteredItems = items.Where(c => c.InventoryCategoryId == categoryId).ToList();
            else
                filteredItems = items;

            Packs = filteredItems.Where(c => c.ItemType == Defaults.ItemTypeOption.Pack.ToString()).ToList();
            Singles = filteredItems.Where(c => c.ItemType == Defaults.ItemTypeOption.Single.ToString()).ToList();

            var cats = await _categoryRepo.GetHavningInventory();
            CategoryList = new SelectList(cats.AsEnumerable(), "InventoryCategoryId","Name", categoryId);

            InventoryStocks = await _inventoryStockRepo.GetAsync(c => c.IsActive);
        }

        public async Task<IActionResult> OnGetDeleteAsync(Guid id)
        {
            var results = await _inventoryActivity.DeleteInventory(id);

            if(!results.IsSuccessful)
                FlashError(results.Message);
            else
                FlashSuccess(results.Message);

            return RedirectToPage("/admin/inventory/index");
        }
    }
}
