﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreLedger.Activities;
using StoreLedger.Model.Repos;
using StoreLedger.Models;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages.Admin.Inventory
{
    [Authorize]
    public class StockForm : BasePageModel
    {
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryActivity _inventoryActivity;
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly InventoryStockRepo _inventoryStockRepo;
        
        public PageForm PageForm { get; set; }

        [BindProperty]
        public InventoryStockViewModel Stock { get; set; }

        public StockForm(InventoryRepo inventory, InventoryActivity inventoryActivity, InventoryCategoryRepo categoryRepo, InventoryStockRepo inventoryStockRepo)
        {
            _inventoryRepo = inventory;
            _inventoryActivity = inventoryActivity;
            _categoryRepo = categoryRepo;
            _inventoryStockRepo = inventoryStockRepo;
        }

        public ActionResult OnGet()
        {
            return RedirectToPage("./index");
        }

        public async Task<IActionResult> OnGetCreateAsync(Guid id)
        {

            PageForm = new PageForm("Add Inventory Stock","create");

            var item = await _inventoryRepo.FindAsync(id);
            if (item == null)
            {
                FlashError("Item not found");
                return RedirectToPage("./index");
            }

            var category = await _categoryRepo.FindAsync(item.InventoryCategoryId);
            var current = await _inventoryStockRepo.GetWithSold(id);

            var price = 0.0M;
            var quantity = 0;
            var sold = 0;
            
            if (current != null)
            {
                price = current.Price;
                quantity = current.Quantity;
                sold = current.Sold;
            }

            Stock = new InventoryStockViewModel
            {
                InventoryId = item.InventoryId,
                Code = item.Code,
                Category = category.Name,
                ImageUrl = item.ImageUrl,
                Name = item.Name,
                PrevQuantity = quantity,
                PrevSold = sold,
                PrevPrice = price,
                Price  = price
            };

            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            PageForm = new PageForm("Add Inventory Stock", "create");

            if (!ModelState.IsValid)
            {
                return RedirectToPage("./stockform", new {handler = "create", id = Stock.InventoryId});
            }

            var results = await _inventoryActivity.CreateStock(Stock);
            if (!results.IsSuccessful)
            {
                FlashWarning(results.Message);
                return RedirectToPage("./stockform", new { handler = "create", id = Stock.InventoryId });
            }
            FlashSuccess(results.Message);

            return RedirectToPage("./index");
        }

        public async Task OnGetEditAsync(int id)
        {
            PageForm = new PageForm("Edit A User", "edit");
            ////AppUser = await _userRepo.FindAsync(id);
            ////var roles = await _userRoleRepo.GetAsync(c => c.RoleId != Defaults.Role.SuperAdminId);
            ////Roles = new SelectList(roles.AsEnumerable(), "RoleId", "Name");
        }

        public async Task<IActionResult> OnPostEditAsync()
        {
            PageForm = new PageForm("Edit A User", "edit");
            ////if (ModelState.IsValid)
            ////{
            ////    var results = await _userActivity.Edit(AppUser);
            ////    if (!results.IsSuccessful)
            ////    {
            ////        this.FlashWarning(results.Message);
            ////        return Page();
            ////    }
            ////    this.FlashSuccess(results.Message);
            ////}

            return RedirectToPage("/admin/inventory/index");
        }
    }
}
