﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages.Admin.Inventory
{
    [Authorize]
    public class ItemForm : BasePageModel
    {
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryActivity _inventoryActivity;
        private readonly InventoryCategoryRepo _categoryRepo;

        public SelectList Categories { get; set; }
        public SelectList ItemTypeOptions { get; set; }
        public PageForm PageForm { get; set; }

        [BindProperty]
        public InventoryViewModel Inventory { get; set; }

        public ItemForm(InventoryRepo inventory, InventoryActivity inventoryActivity, InventoryCategoryRepo categoryRepo)
        {
            _inventoryRepo = inventory;
            _inventoryActivity = inventoryActivity;
            _categoryRepo = categoryRepo;
            Inventory = new InventoryViewModel { InventoryId = new Guid()};
            ItemTypeOptions = new SelectList(new[]
                {
                    new {Value = Defaults.ItemTypeOption.Single, Name = Defaults.ItemTypeOption.Single},
                    new {Value = Defaults.ItemTypeOption.Pack, Name = Defaults.ItemTypeOption.Pack}
                },
                "Value",
                "Name",
                0
            );
        }

        public ActionResult OnGet()
        {
            return RedirectToPage("./index");
        }

        public async Task OnGetCreateAsync(string itemType)
        {

            PageForm = new PageForm("Add Inventory Item","create");
            var cats = await _categoryRepo.GetAllAsync();
            Categories = new SelectList(cats.AsEnumerable(), "InventoryCategoryId", "Name");
            if (!string.IsNullOrEmpty(itemType))
            {
                ItemTypeOptions = new SelectList(new[]
                    {
                        new {Value = Defaults.ItemTypeOption.Single, Name = Defaults.ItemTypeOption.Single},
                        new {Value = Defaults.ItemTypeOption.Pack, Name = Defaults.ItemTypeOption.Pack}
                    },
                    "Value",
                    "Name",
                    itemType
                );
            }

            var inCounts = await _inventoryRepo.GetAllAsync();
            Inventory.Code = inCounts.Count + 1;
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            PageForm = new PageForm("Add Inventory Item", "create");

            if (!ModelState.IsValid) return Page();

            var results = await _inventoryActivity.Create(Inventory);
            if (!results.IsSuccessful)
            {
                FlashWarning(results.Message);
                return Page();
            }
            FlashSuccess(results.Message);

            return RedirectToPage("./stockform", new { handler = "create", id = results.State});
        }

        public async Task<IActionResult> OnGetEditAsync(Guid id)
        {
            PageForm = new PageForm("Edit Inventory Item", "edit");
            var item = await _inventoryRepo.FindAsync(id);
            if (item == null) return RedirectToPage("./index");

            Inventory.Name = item.Name;
            Inventory.Code = item.Code;
            Inventory.ImageUrl = item.ImageUrl;
            Inventory.MaxLevel = item.MaxLevel;
            Inventory.MinLevel = item.MinLevel;
            Inventory.ReOrderLevel = item.ReOrderLevel;
            Inventory.InventoryCategoryId = item.InventoryCategoryId;
            Inventory.InventoryId = item.InventoryId;
            Inventory.ItemType = item.ItemType;

            var cats = await _categoryRepo.GetAllAsync();
            Categories = new SelectList(cats.AsEnumerable(), "InventoryCategoryId", "Name");

            return Page();
        }

        public async Task<IActionResult> OnPostEditAsync()
        {
            PageForm = new PageForm("Edit Inventory Item", "edit");
            if (!ModelState.IsValid)
            {
                var cats = await _categoryRepo.GetAllAsync();
                Categories = new SelectList(cats.AsEnumerable(), "InventoryCategoryId", "Name");
                return Page();
            }

            var results = await _inventoryActivity.Edit(Inventory);
            if (!results.IsSuccessful)
            {
                FlashWarning(results.Message);
                return Page();
            }
            FlashSuccess(results.Message);

            return RedirectToPage("./index");
        }
    }
}
