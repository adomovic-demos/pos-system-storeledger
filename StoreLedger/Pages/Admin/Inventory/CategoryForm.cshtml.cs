﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;

namespace StoreLedger.Pages.Admin.Inventory
{
    [Authorize]
    public class AdminInventoryCategoryForm : BasePageModel
    {
        private readonly InventoryActivity _inventoryActivity;
        private readonly InventoryCategoryRepo _categoryRepo;

        public PageForm PageForm { get; set; }

        [BindProperty]
        public InventoryCategory Category { get; set; }

        public AdminInventoryCategoryForm(InventoryActivity inventoryActivity, InventoryCategoryRepo categoryRepo)
        {
            _inventoryActivity = inventoryActivity;
            _categoryRepo = categoryRepo;
            Category = new InventoryCategory{ InventoryCategoryId = 0};
        }

        public ActionResult OnGet()
        {
            return RedirectToPage("/admin/inventory/category");
        }

        public async Task OnGetCreateAsync()
        {

            PageForm = new PageForm("Add A Category","create");
            
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            PageForm = new PageForm("Add A Category", "create");

            if (ModelState.IsValid)
            {
                var results = await _inventoryActivity.CreateCategory(Category);
                if (!results.IsSuccessful)
                {
                    FlashWarning(results.Message);
                    return Page();
                }
                FlashSuccess(results.Message);
            }

            return RedirectToPage("/admin/inventory/category");
        }

        public async Task OnGetEditAsync(int id)
        {
            PageForm = new PageForm("Edit A Category", "edit");
            Category = await _categoryRepo.FindAsync(id);
            
        }

        public async Task<IActionResult> OnPostEditAsync()
        {
            PageForm = new PageForm("Edit A Category", "edit");
            if (ModelState.IsValid)
            {
                var results = await _inventoryActivity.EditCategory(Category);
                if (!results.IsSuccessful)
                {
                    FlashWarning(results.Message);
                    return Page();
                }
                FlashSuccess(results.Message);
            }

            return RedirectToPage("/admin/inventory/category");
        }
    }
}
