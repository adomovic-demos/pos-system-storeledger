﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages.Admin.Inventory
{
    [Authorize]
    public class ObsoleteForm : BasePageModel
    {
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryActivity _inventoryActivity;
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly InventoryStockRepo _inventoryStockRepo;

        public PageForm PageForm { get; set; }

        public InventoryStockViewModel Stock { get; set; }

        [BindProperty]
        public ObsoleteStockViewModel Obsolete { get; set; }

        public SelectList ObsoleteTypes { get; set; }

        public ObsoleteForm(InventoryRepo inventory, InventoryActivity inventoryActivity, 
            InventoryCategoryRepo categoryRepo, 
            InventoryStockRepo inventoryStockRepo)
        {
            _inventoryRepo = inventory;
            _inventoryActivity = inventoryActivity;
            _categoryRepo = categoryRepo;
            _inventoryStockRepo = inventoryStockRepo;

            ObsoleteTypes = new SelectList(new[]
                {
                    new {Value = Defaults.ObsoleteType.Expired, Name = Defaults.ObsoleteType.Expired},
                    new {Value = Defaults.ObsoleteType.Damaged, Name = Defaults.ObsoleteType.Damaged},
                    new {Value = Defaults.ObsoleteType.FactoryDefect, Name = Defaults.ObsoleteType.FactoryDefect}
                },
                "Value",
                "Name",
                0
            );
        }

        public ActionResult OnGet()
        {
            return RedirectToPage("./index");
        }

        public async Task<IActionResult> OnGetCreateAsync(Guid id)
        {

            PageForm = new PageForm("Obsolete Stock","create");

            var item = await _inventoryRepo.FindAsync(id);
            if (item == null)
            {
                FlashError("Item not found");
                return RedirectToPage("./index");
            }

            var category = await _categoryRepo.FindAsync(item.InventoryCategoryId);
            var current = await _inventoryStockRepo.GetWithSold(id);

            if (current == null)
            {
                FlashError("Item not found");
                return RedirectToPage("./index");
            }

            Stock = new InventoryStockViewModel
            {
                InventoryId = item.InventoryId,
                Code = item.Code,
                Category = category.Name,
                ImageUrl = item.ImageUrl,
                Name = item.Name,
                PrevQuantity = current.Quantity,
                PrevSold = current.Sold,
                PrevPrice = current.Price,
                Price = current.Price
            };

            Obsolete = new ObsoleteStockViewModel
            {
                InventoryId = item.InventoryId,
                InventoryStockId = current.InventoryStockId,
                Price = current.Price
            };
            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            PageForm = new PageForm("Obsolete Stock", "create");

            if (!ModelState.IsValid)
            {
                return RedirectToPage("./obsoleteform", new {handler = "create", id = Stock.InventoryId});
            }

            var results = await _inventoryActivity.CreateObsoleteStock(Obsolete);
            if (!results.IsSuccessful)
            {
                FlashWarning(results.Message);
                return RedirectToPage("./obsoleteform", new { handler = "create", id = Stock.InventoryId });
            }
            FlashSuccess(results.Message);

            return RedirectToPage("./index");
        }

        public async Task OnGetEditAsync(int id)
        {
            PageForm = new PageForm("Edit A User", "edit");
            ////AppUser = await _userRepo.FindAsync(id);
            ////var roles = await _userRoleRepo.GetAsync(c => c.RoleId != Defaults.Role.SuperAdminId);
            ////Roles = new SelectList(roles.AsEnumerable(), "RoleId", "Name");
        }

        public async Task<IActionResult> OnPostEditAsync()
        {
            PageForm = new PageForm("Edit A User", "edit");
            ////if (ModelState.IsValid)
            ////{
            ////    var results = await _userActivity.Edit(AppUser);
            ////    if (!results.IsSuccessful)
            ////    {
            ////        this.FlashWarning(results.Message);
            ////        return Page();
            ////    }
            ////    this.FlashSuccess(results.Message);
            ////}

            return RedirectToPage("/admin/inventory/index");
        }
    }
}
