﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;

namespace StoreLedger.Pages.Admin.Inventory
{
    [Authorize]
    public class AdminInventoryCategoryPage : BasePageModel
    {
        private readonly InventoryActivity _inventoryActivity;
        public List<InventoryCategory> Categories { get; set; }
        private readonly InventoryCategoryRepo _categoryRepo;

        public AdminInventoryCategoryPage(InventoryActivity inventoryActivity, InventoryCategoryRepo categoryRepo)
        {
            _inventoryActivity = inventoryActivity;
            _categoryRepo = categoryRepo;
            Categories = new List<InventoryCategory>();
        }

        public async Task OnGetAsync()
        {
            var results = await _categoryRepo.GetAllAsync();
            Categories = results.OrderBy(c => c.Name).ToList();
        }

        public async Task<IActionResult> OnGetDeleteAsync(int id)
        {

            var results = await _inventoryActivity.DeleteCategory(id);

            if (!results.IsSuccessful)
            {
                FlashError(results.Message);
            }
            else
                FlashSuccess("Category Deleted");

            return RedirectToPage("/admin/inventory/category");
        }
    }
}
