﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;

namespace StoreLedger.Pages.Admin.Users
{
    [Authorize(Roles = Defaults.Role.AllAdmin)]
    public class AdminUsersPage : BasePageModel
    {
        private readonly UserRepo _userRepo;
        private readonly UserRoleRepo _userRoleRepo;
        private readonly UserActivity _userActivity;

        public AdminUsersPage(UserRepo userRepo, UserRoleRepo userRoleRepo, UserActivity userActivity)
        {
            _userRepo = userRepo;
            _userRoleRepo = userRoleRepo;
            _userActivity = userActivity;
            Users = new List<User>();
        }

        public string Message { get; set; }
        public SelectList Roles { get; set; }

        public List<User> Users { get; set; }


        public async Task OnGetAsync()
        {
            Users = await _userRepo.GetAsync(c => c.UserId != 1, c => c.UserRole);
        }

        public async Task<IActionResult> OnGetDeleteAsync(int id)
        {
            var results = await _userActivity.Delete(HttpContext.User.Identity.Name, id);
            if(!results.IsSuccessful)
                FlashError(results.Message);
            else
                FlashSuccess("User Deleted");

            return RedirectToPage("./index");
        }
    }
}
