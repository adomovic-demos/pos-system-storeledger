﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;

namespace StoreLedger.Pages.Admin.Users
{
    [Authorize]
    public class AdminUsersForm : BasePageModel
    {
        private readonly UserRepo _userRepo;
        private readonly UserRoleRepo _userRoleRepo;
        private readonly UserActivity _userActivity;

        public SelectList Roles { get; set; }
        public SelectList StatusOption { get; set; }
        public PageForm PageForm { get; set; }

        [BindProperty]
        public User AppUser { get; set; }

        public AdminUsersForm(UserRepo userRepo, UserRoleRepo userRoleRepo, UserActivity userActivity)
        {
            _userRepo = userRepo;
            _userRoleRepo = userRoleRepo;
            _userActivity = userActivity;
            StatusOption = new SelectList((new[]
                {
                    new {Value = true, Name = "Yes"},
                    new {Value = false, Name = "No"}
                }),
                "Value",
                "Name",
                0
            );
            AppUser = new User{ UserId = 0};
        }

        public ActionResult OnGet()
        {
            return RedirectToPage("./index");
        }

        public async Task OnGetCreateAsync()
        {

            PageForm = new PageForm("Add A User","create");
            var roles = await _userRoleRepo.GetAsync(c => c.RoleId != Defaults.Role.SuperAdminId);
            Roles = new SelectList(roles.AsEnumerable(), "RoleId", "Name");
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            PageForm = new PageForm("Add A User", "create");

            if (ModelState.IsValid)
            {
                var results = await _userActivity.Create(AppUser);
                if (!results.IsSuccessful)
                {
                    FlashWarning(results.Message);
                    return Page();
                }
                FlashSuccess(results.Message);
            }

            return RedirectToPage("./index");
        }

        public async Task OnGetEditAsync(int id)
        {
            PageForm = new PageForm("Edit A User", "edit");
            AppUser = await _userRepo.FindAsync(id);
            var roles = await _userRoleRepo.GetAsync(c => c.RoleId != Defaults.Role.SuperAdminId);
            Roles = new SelectList(roles.AsEnumerable(), "RoleId", "Name");
        }

        public async Task<IActionResult> OnPostEditAsync()
        {
            PageForm = new PageForm("Edit A User", "edit");
            if (ModelState.IsValid)
            {
                var results = await _userActivity.Edit(AppUser);
                if (!results.IsSuccessful)
                {
                    FlashWarning(results.Message);
                    return Page();
                }
                FlashSuccess(results.Message);
            }

            return RedirectToPage("./index");
        }
    }
}
