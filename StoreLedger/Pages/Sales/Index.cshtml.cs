﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages.Sales
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly InventoryRepo _inventoryRepo;

        public IndexModel(InventoryCategoryRepo categoryRepo, InventoryRepo inventoryRepo)
        {
            _categoryRepo = categoryRepo;
            _inventoryRepo = inventoryRepo;
            Categories = new List<InventoryCategory>();
            Inventories = new List<InventoryView>();
            //Sales = new List<InventorySaleViewModel>();
        }

        public List<InventoryCategory> Categories { get; set; }
        public List<InventoryView> Inventories { get; set; }
        public List<InventoryView> InventoriesForSale { get; set; }

        //[BindProperty]
        //public List<InventorySaleViewModel> Sales { get; set; }

        public async Task OnGetAsync()
        {
            Categories = await _categoryRepo.GetHavningInventory();
            Inventories = await _inventoryRepo.GetWithStock();
            InventoriesForSale = await _inventoryRepo.GetWithStockForSales();
        }

        //public async Task<IActionResult> OnPostSellAsync(List<SaleItemViewModel> sales)
        //{
        //    var saleitems = sales;


        //    //save to temp tr
        //    return RedirectToPage("./index");
        //}
    }
}