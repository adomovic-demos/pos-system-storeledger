﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StoreLedger.Activities;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Pages.Sales
{
    [Authorize]
    public class PayModel : BasePageModel
    {
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly InventoryRepo _inventoryRepo;
        private readonly TransactionActivity _transActivity;

        public PayModel(InventoryCategoryRepo categoryRepo, InventoryRepo inventoryRepo, TransactionActivity transActivity)
        {
            _categoryRepo = categoryRepo;
            _inventoryRepo = inventoryRepo;
            _transActivity = transActivity;
            Categories = new List<InventoryCategory>();
            Inventories = new List<InventoryView>();
            //Sales = new List<InventorySaleViewModel>();
        }

        public List<InventoryCategory> Categories { get; set; }
        public List<InventoryView> Inventories { get; set; }

        //[BindProperty]
        public List<SaleItemViewModel> Sales { get; set; }

        public async Task OnGetAsync()
        {
            
        }

        public async Task<IActionResult> OnPostSellAsync(List<SaleItemViewModel> model)
        {
            if (!model.Any())
            {
                FlashError("Select items to sell");
                return RedirectToPage("./index");
            }
            var inventory = await _inventoryRepo.GetWithStock();
            if(!inventory.Any()) return RedirectToPage("./index");

            foreach (var item in model)
            {
                var currInventory = inventory.FirstOrDefault(c => c.InventoryId == item.InventoryId);
                if (currInventory == null) continue;
                item.Name = currInventory.Name;
                item.Code = currInventory.Code;
                item.Price = currInventory.Price;
                item.Total = item.Price * item.Quantity;
            }

            Sales = model;

            return Page();
        }

        public async Task<IActionResult> OnPostProcessTransactionAsync(SaleViewModel model)
        {
            var userId =
                Convert.ToInt32(HttpContext.User.Claims.FirstOrDefault(c => c.Type == Defaults.UserType.Id)?.Value);
            var results = await _transActivity.ProcessTransaction(userId, model);
            if (results.IsSuccessful)
            {
                FlashSuccess("Payment completed successfully");
                return RedirectToPage("./index");
            }

            FlashError(results.Message);
            return Page();
        }
    }
}