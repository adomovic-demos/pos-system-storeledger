﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StoreLedger.Model;
using StoreLedger.Model.Repos;

namespace StoreLedger.Pages.Reports
{
    [Authorize]
    public class AggregateModel : PageModel
    {
        private readonly TransactionRepo _transactionRepo;

        public List<TransactionAggregate> Transactions { get; set; }
        public DateTime? From { get; set; }
        public DateTime? To { get; set; }

        public AggregateModel(TransactionRepo transactionRepo)
        {
            _transactionRepo = transactionRepo;

            Transactions = new List<TransactionAggregate>();
        }

        public async Task OnGetAsync(DateTime? from, DateTime? to)
        {
            var currDate = DateTime.Today;
            if (from == null && to == null)
            {
                from = currDate.AddDays(-90);
                to = currDate;
            }
            Transactions = await _transactionRepo.GetAggregateReport(from, to);
            From = from;
            To = to;
        }
    }
}