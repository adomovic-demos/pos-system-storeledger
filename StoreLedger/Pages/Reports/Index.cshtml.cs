﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using StoreLedger.Model;
using StoreLedger.Model.Repos;

namespace StoreLedger.Pages.Reports
{
    [Authorize]
    public class IndexModel : PageModel
    {
        private readonly TransactionRepo _transactionRepo;
        private readonly TransactionItemRepo _transactionItemRepo;
        private readonly InventoryRepo _inventoryRepo;
        private readonly UserRepo _userRepo;

        public List<TransactionItem> TransactionItems { get; set; }
        public List<Model.Inventory> Inventories { get; set; } 
        public DateTime CurrentDate { get; set; }
        public List<User> Users { get; set; }

        public IndexModel(TransactionRepo transactionRepo, TransactionItemRepo transactionItemRepo, 
            InventoryRepo inventoryRepo, UserRepo userRepo)
        {
            _transactionRepo = transactionRepo;
            _transactionItemRepo = transactionItemRepo;
            _inventoryRepo = inventoryRepo;
            _userRepo = userRepo;

            TransactionItems = new List<TransactionItem>();
        }

        public async Task OnGetAsync(DateTime? currDate)
        {
            CurrentDate = currDate != null ?
                DateTime.Parse(currDate.ToString()) :
                DateTime.Today;

            TransactionItems = await _transactionItemRepo.GetDayReport(CurrentDate);
            Inventories = await _inventoryRepo.GetAllAsync();
            Users = await _userRepo.GetAllAsync();
        }
    }
}