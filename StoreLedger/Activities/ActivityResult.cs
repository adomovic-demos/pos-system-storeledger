namespace StoreLedger.Activities
{
    public class ActivityResult
    {
        public ActivityResult(bool isSuccessful, string message)
        {
            IsSuccessful = isSuccessful;
            Message = message;
        }

        public ActivityResult(bool isSuccessful, string message, dynamic state)
        {
            IsSuccessful = isSuccessful;
            Message = message;
            State = state;
        }

        public bool IsSuccessful { get; set; }
        public string Message { get; set; }
        public string Description { get; set; }

        //Additional data
        public dynamic State { get; set; }
    }
}
