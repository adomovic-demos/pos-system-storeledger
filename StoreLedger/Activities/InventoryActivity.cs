﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models.ViewModels;

namespace StoreLedger.Activities
{
    public class InventoryActivity
    {
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly IHostingEnvironment _environment;
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryStockRepo _inventoryStockRepo;
        private readonly ObsoleteStockRepo _obsoleteStockRepo;

        public InventoryActivity(InventoryCategoryRepo categoryRepo, IHostingEnvironment environment, InventoryRepo inventoryRepo, 
            InventoryStockRepo inventoryStockRepo, ObsoleteStockRepo obsoleteStockRepo)
        {
            _categoryRepo = categoryRepo;
            _environment = environment;
            _inventoryRepo = inventoryRepo;
            _inventoryStockRepo = inventoryStockRepo;
            _obsoleteStockRepo = obsoleteStockRepo;
        }

        public async Task<ActivityResult> Create(InventoryViewModel model)
        {
            var existing = await _inventoryRepo.FirstOrDefaultAsync(c => c.Name.ToLower() == model.Name.ToLower());
            if(existing != null) return new ActivityResult(false,$"Inventory with name {model.Name} already exists");

            if(model.ImageFile == null) return new ActivityResult(false, "Select image");

            //if(model.Quantity < 0) return new ActivityResult(false, $"Item quantity cannot be lesser than zero");
            //if (model.Price < 0) return new ActivityResult(false, $"Item price cannot be lesser than zero");
            if(model.MinLevel < 0) return new ActivityResult(false, $"Item min. level cannot be lesser than zero");
            if (model.MaxLevel < 0) return new ActivityResult(false, $"Item max. level cannot be lesser than zero");

            if(model.ImageFile == null) return new ActivityResult(false, "Select item image");

            if(!FileIsValid(model.ImageFile)) return new ActivityResult(false, $"Item image can only be .jpg, .gif, or .png file");

            //upload
            var imageName = Guid.NewGuid() + Path.GetExtension(model.ImageFile.FileName);
            
            var imagePath = Path.Combine(_environment.WebRootPath, "uploads", imageName);
            using (var fileStream = new FileStream(imagePath, FileMode.Create))
            {
                await model.ImageFile.CopyToAsync(fileStream);
            }

            var inventory = new Inventory
            {
                Code = model.Code,
                Name = model.Name,
                DateCreated = DateTime.Today,
                ImageUrl = $"/uploads/{imageName}",
                MaxLevel = model.MaxLevel,
                MinLevel = model.MinLevel,
                UserId = 1,
                IsEnabled = true,
                ItemType = model.ItemType,
                InventoryCategoryId = model.InventoryCategoryId,
                InventoryId = Guid.NewGuid(),
                ReOrderLevel = model.ReOrderLevel
            };
            await _inventoryRepo.InsertAsync(inventory);

            return new ActivityResult(true, $"Inventory {model.Name} created", inventory.InventoryId);
        }

        public async Task<ActivityResult> Edit(InventoryViewModel model)
        {
            var existing = await _inventoryRepo
                .FirstOrDefaultAsync(c => c.Name.ToLower() == model.Name.ToLower() && c.InventoryId != model.InventoryId);

            if (existing != null) return new ActivityResult(false, $"Inventory with name {model.Name} already exists");

            if (model.MinLevel < 0) return new ActivityResult(false, $"Item min. level cannot be lesser than zero");
            if (model.MaxLevel < 0) return new ActivityResult(false, $"Item max. level cannot be lesser than zero");

            var imageUrl = model.ImageUrl;
            if (model.ImageFile != null)
            {
                if (!FileIsValid(model.ImageFile)) return new ActivityResult(false, $"Item image can only be .jpg, .gif, or .png file");

                //upload
                var imageName = Guid.NewGuid() + Path.GetExtension(model.ImageFile.FileName);

                var imagePath = Path.Combine(_environment.WebRootPath, "uploads", imageName);
                using (var fileStream = new FileStream(imagePath, FileMode.Create))
                {
                    await model.ImageFile.CopyToAsync(fileStream);
                }

                imageUrl = $"/uploads/{imageName}";
            }

            var inventory = await _inventoryRepo.FindAsync(model.InventoryId);
            inventory.Name = model.Name;
            inventory.ImageUrl = imageUrl;
            inventory.MaxLevel = model.MaxLevel;
            inventory.MinLevel = model.MinLevel;
            inventory.UserId = 1;
            inventory.ItemType = model.ItemType;
            inventory.InventoryCategoryId = model.InventoryCategoryId;
            inventory.ReOrderLevel = model.ReOrderLevel;
        
            await _inventoryRepo.UpdateAsync(inventory);

            return new ActivityResult(true, $"Inventory {model.Name} updated", inventory.InventoryId);
        }


        public async Task<ActivityResult> CreateStock(InventoryStockViewModel model)
        {
            if(model.Quantity <= 0) return new ActivityResult(false, $"Item quantity cannot be lesser than zero");
            if (model.Price <= 0) return new ActivityResult(false, $"Item price cannot be lesser than zero");

            //set current to inactive
            var current = await _inventoryStockRepo
                .FirstOrDefaultAsync(c => c.InventoryId == model.InventoryId 
                && c.IsActive, false);
            if (current != null)
            {
                current.IsActive = false;
                await _inventoryStockRepo.UpdateAsync(current);
            }

            //set new stock
            var stock = new InventoryStock
            {
                DateCreated = DateTime.Today,
                InventoryId = model.InventoryId,
                InventoryStockId = Guid.NewGuid(),
                Price = model.Price,
                Quantity = model.Quantity,
                IsActive = true
            };
            await _inventoryStockRepo.InsertAsync(stock);
            
            return new ActivityResult(true, $"Inventory stock created");
        }

        public async Task<ActivityResult> CreateObsoleteStock(ObsoleteStockViewModel model)
        {
            if (model.Quantity <= 0) return new ActivityResult(false, $"Item quantity cannot be lesser than zero");
            if (model.ObsoleteQuantity <= 0) return new ActivityResult(false, $"Item obsolete quantity cannot be lesser than zero");

            //set current stock to inactive
            var current = await _inventoryStockRepo
                .FirstOrDefaultAsync(c => c.InventoryId == model.InventoryId
                                          && c.IsActive, false);
            if (current != null)
            {
                current.IsActive = false;
                await _inventoryStockRepo.UpdateAsync(current);
            }

            //set new stock
            var stock = new InventoryStock
            {
                DateCreated = DateTime.Now,
                InventoryId = model.InventoryId,
                InventoryStockId = Guid.NewGuid(),
                Price = model.Price,
                Quantity = model.Quantity,
                IsActive = true
            };
            await _inventoryStockRepo.InsertAsync(stock);

            //save obsolete
            var obsolete = new ObsoleteStock
            {
                DateCreated = DateTime.Now,
                InventoryId = model.InventoryId,
                Notes = model.Notes,
                InventoryStockId = model.InventoryStockId,
                ObsoleteStockId = Guid.NewGuid(),
                ObsoleteType = model.ObsoleteType,
                Price = model.Price,
                Quantity = model.ObsoleteQuantity
            };
            await _obsoleteStockRepo.InsertAsync(obsolete);

            return new ActivityResult(true, $"Obsolete stock recorded");
        }

        bool FileIsValid(IFormFile file)
        {
            if (file == null) return false;
            var extension = Path.GetExtension(file.FileName);
            if (extension == null) return false;

            var fudExt = extension.ToLower();
            string[] allowedExtensions = { ".gif", ".jpg", ".png", ".jpeg" };
            return allowedExtensions.Any(t => fudExt == t);
        }
        //bool HomepageImageDimensionValid(IFormFile file)
        //{
        //    if (file!=null)
        //    {
        //        var imgStream = file.OpenReadStream();
        //        var newImage = new System.imag Bitmap(1024, 768);
        //        using (var g = Graphics.FromImage(newImage))
        //        {
        //            g.DrawImage(image, 0, 0, 1024, 768);
        //        }
        //        if (currImg.Width >= 1170 && currImg.Height == 250)
        //            return true;
        //    }
        //    return false;
        //}

        //bool InnerImageDimensionValid(HttpPostedFileBase file)
        //{
        //    if (file.ContentLength > 0)
        //    {
        //        var imgStream = file.InputStream;
        //        var currImg = Image.FromStream(imgStream);
        //        if (currImg.Width >= 1170 && currImg.Height == 250)
        //            return true;
        //    }
        //    return false;
        //}

        public async Task<ActivityResult> CreateCategory(InventoryCategory model)
        {
            var existing = await _categoryRepo.FirstOrDefaultAsync(c => c.Name.ToLower() == model.Name.ToLower());
            if(existing != null) return new ActivityResult(false, $"Category with name {model.Name} already exists");

            await _categoryRepo.InsertAsync(model);

            return new ActivityResult(true, $"Cateory {model.Name} created");
        }

        public async Task<ActivityResult> EditCategory(InventoryCategory model)
        {
            var category = await _categoryRepo.FindAsync(model.InventoryCategoryId);
            if (category == null) return new ActivityResult(false, $"Category with name {model.Name} does not exists");

            category.Name = model.Name;
            await _categoryRepo.UpdateAsync(category);

            return new ActivityResult(true, $"Cateory {model.Name} updated");
        }

        public async Task<ActivityResult> DeleteCategory(int id)
        {
            var category = await _categoryRepo.FindAsync(id);
            if (category == null) return new ActivityResult(false, "Cannot delete category");

            var inventory = await _inventoryRepo.FirstOrDefaultAsync(c => c.InventoryCategoryId == id);
            if(inventory != null)
                return new ActivityResult(false, $"Category {category.Name} already has inventory");

            await _categoryRepo.DeleteAsync(id);

            return new ActivityResult(true, $"Cateory {category.Name} deleted");
        }

        public async Task<ActivityResult> DeleteInventory(Guid id)
        {
            var inventory = await _inventoryRepo.FindAsync(id);
            if (inventory == null) return new ActivityResult(false, "Cannot delete inventory");

            var stock = await _inventoryStockRepo.FirstOrDefaultAsync(c => c.InventoryId == id);
            if (stock != null)
                return new ActivityResult(false, $"Inventory {inventory.Name} already has stock");

            await _inventoryRepo.DeleteAsync(id);

            return new ActivityResult(true, $"Inventory {inventory.Name} deleted");
        }
    }
}
