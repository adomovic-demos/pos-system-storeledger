﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using StoreLedger.Model;
using StoreLedger.Model.Repos;

namespace StoreLedger.Activities
{
    public class UserActivity
    {
        private readonly UserRepo _userRepo;
        private readonly TransactionRepo _transactionRepo;

        public UserActivity(UserRepo userRepo, TransactionRepo transactionRepo)
        {
            _userRepo = userRepo;
            _transactionRepo = transactionRepo;
        }

        public async Task<ActivityResult> Create(User model)
        {
            var existing = await _userRepo.GetAsync(c => c.Username.ToLower() == model.Username.ToLower());
            if(existing.Any()) return new ActivityResult(false, $"Username {model.Username} already exists");

            await _userRepo.InsertAsync(model);

            return new ActivityResult(true, $"User with name: {model.FirstName} {model.LastName} created");
        }

        public async Task<ActivityResult> Edit(User model)
        {
            var user = await _userRepo.FindAsync(model.UserId);
            if (user == null) return new ActivityResult(false, "User not found");

            user.FirstName = model.FirstName;
            user.LastName = model.LastName;
            user.RoleId = model.RoleId;
            user.MobileNumber = model.MobileNumber;
            user.Password = model.Password;
            user.IsEnabled = model.IsEnabled;

            await _userRepo.UpdateAsync(user);

            return new ActivityResult(true, $"User with name: {model.FirstName} {model.LastName} updated");
        }

        public async Task<ActivityResult> Delete(string username, int id)
        {
            var existing = await _userRepo.FindAsync(id);
            
            //todo: disassociate user from transactions
            var trans = await _transactionRepo.FirstOrDefaultAsync(c => c.Userid == existing.UserId);
            if(trans != null)
                return new ActivityResult(false, $"User {existing.FirstName} {existing.LastName} already has transactions");

            if (existing != null && 
                existing.RoleId != Defaults.Role.SuperAdminId && 
                existing.Username.ToLower() != username.ToLower()
                )
            {
                await _userRepo.DeleteAsync(id);
                return new ActivityResult(true, $"User {existing.FirstName}  {existing.LastName}deleted");
            }
            return new ActivityResult(false, "Cannot delete user");
        }
    }
}
