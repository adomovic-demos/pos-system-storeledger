﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using StoreLedger.Model;
using StoreLedger.Model.Repos;
using StoreLedger.Models.ViewModels;
using Transaction = StoreLedger.Model.Transaction;

namespace StoreLedger.Activities
{
    public class TransactionActivity
    {
        private readonly InventoryCategoryRepo _categoryRepo;
        private readonly IHostingEnvironment _environment;
        private readonly InventoryRepo _inventoryRepo;
        private readonly InventoryStockRepo _inventoryStockRepo;
        private readonly TransactionItemRepo _transactionItemRepo;
        private readonly TransactionRepo _transactionRepo;

        public TransactionActivity(InventoryCategoryRepo categoryRepo, IHostingEnvironment environment, InventoryRepo inventoryRepo, 
            InventoryStockRepo inventoryStockRepo, TransactionItemRepo transactionItemRepo, TransactionRepo transactionRepo)
        {
            _categoryRepo = categoryRepo;
            _environment = environment;
            _inventoryRepo = inventoryRepo;
            _inventoryStockRepo = inventoryStockRepo;
            _transactionItemRepo = transactionItemRepo;
            _transactionRepo = transactionRepo;
        }

        public async Task<ActivityResult> ProcessTransaction(int userId, SaleViewModel model)
        {
            var transModel = new TransactionViewModel
            {
                Items = JsonConvert.DeserializeObject<List<SaleItemViewModel>>(model.Items),
                AmountPaid = model.AmountPaid
            };
            var inventory = await _inventoryRepo.GetWithStock();
            var transItems = new List<TransactionItem>();

            var date = DateTime.UtcNow;
            var transId = Guid.NewGuid();

            foreach (var item in transModel.Items)
            {
                var currInventory = inventory.FirstOrDefault(c => c.InventoryId == item.InventoryId);
                if (currInventory == null) continue;
                //item.Name = currInventory.Name;
                //item.Code = currInventory.Code;
                //item.Price = currInventory.Price;
                //item.Total = item.Price * item.Quantity;

                //trans
                var total = currInventory.Price * item.Quantity;
                transItems.Add(new TransactionItem
                {
                    DateCreated = date,
                    InventoryStockId = item.InventoryStockId,
                    InventoryId = item.InventoryId,
                    Price = currInventory.Price,
                    Quantity = item.Quantity,
                    Total = total,
                    TransactionId = transId,
                    TransactionItemId = Guid.NewGuid()
                });

                transModel.AmountCharged += total;
            }

            if(transModel.AmountCharged > transModel.AmountPaid)
                return new ActivityResult(false,$"Amount paid: {transModel.AmountPaid}, cannot the lesser than amount charged: {transModel.AmountCharged}");

            
            var transaction = new Transaction
            {
                AmountCharged = transModel.AmountCharged,
                AmountChange = transModel.AmountPaid - transModel.AmountCharged,
                AmountPaid = transModel.AmountPaid,
                CustomerId = new Guid(),
                DateCreated = date,
                PaymentMethod = Defaults.PaymentMethod.Cash.ToString(),
                TransactionId = transId,
                Userid = userId
            };

            using (var trans = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                foreach (var item in transItems)
                {
                    await _transactionItemRepo.InsertAsync(item);
                }
                await _transactionRepo.InsertAsync(transaction);
                trans.Complete();
            }
            

            return new ActivityResult(true, string.Empty);
        }
    }
}
