﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using StoreLedger.Model;
using StoreLedger.Model.Repos;

namespace StoreLedger.Activities
{
    public class LoginActivity
    {
        private readonly UserRepo _userRepo;
        private readonly IConfiguration _config;

        public LoginActivity(UserRepo userRepo, IConfiguration config)
        {
            _userRepo = userRepo;
            _config = config;
        }

        public async Task<ActivityResult> Login(HttpContext context, string username, string password)
        {
            var user = await _userRepo.FirstOrDefaultAsync(c => 
                c.Username.ToLower() == username.ToLower() && 
                c.Password == password,a => a.UserRole);

            if(user == null)
                return new ActivityResult(false, "Username or Password not valid");

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.MobilePhone, user.MobileNumber),
                new Claim(ClaimTypes.Name, user.Username),
                new Claim(ClaimTypes.Role, user.RoleId.ToString()),
                new Claim(Defaults.RoleType.FullName,$"{user.FirstName} {user.LastName}"),
                new Claim(ClaimTypes.Role,$"{user.UserRole.Name}"),
                new Claim(Defaults.RoleType.RoleName, user.UserRole.Name),
                new Claim(Defaults.UserType.Id, user.UserId.ToString())
            };

            var userIdentity = new ClaimsIdentity(claims, CookieAuthenticationDefaults.AuthenticationScheme);
            var userPrincipal = new ClaimsPrincipal(userIdentity);

            await context.SignInAsync(
                scheme: CookieAuthenticationDefaults.AuthenticationScheme,
                principal: userPrincipal,
                properties: new AuthenticationProperties
                {
                    IsPersistent = true,
                    ExpiresUtc = DateTime.Now.AddMinutes(Convert.ToDouble(_config["SessionTimeoutInMinutes"])),

                });
            
            return new ActivityResult(true, string.Empty);
        }

    }
}
