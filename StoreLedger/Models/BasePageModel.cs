﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures.Internal;
using Newtonsoft.Json;

namespace StoreLedger.Models
{
    public class BasePageModel : PageModel
    {
        
        public void FlashWarning(string message)
        {
            var flashMessage = new Message
            {
                Type = MessageTypes.Warning,
                Body = message.Trim()
            };

            PersistMessage(flashMessage);
        }

        public void FlashSuccess(string message)
        {
            var flashMessage = new Message
            {
                Type = MessageTypes.Success,
                Body = message.Trim()
            };

            PersistMessage(flashMessage);
        }

        public void FlashError(string message)
        {
            var flashMessage = new Message
            {
                Type = MessageTypes.Error,
                Body = message.Trim()
            };

            PersistMessage(flashMessage);
        }

        private void PersistMessage(Message flashMessage)
        {
            HttpContext.Session.SetString("FlashMsg", JsonConvert.SerializeObject(flashMessage));
            //TempData["flash"] = JsonConvert.SerializeObject(flashMessage);
        }
    }

    public class Message
    {
        public string Type { get; set; }
        public string Body { get; set; }
        public bool Dismissible { get; set; }
        public Message()
        {
            Dismissible = true;
        }
    }

    public class MessageTypes
    {
        public const string Success = "alert-success";
        public const string Warning = "alert-warning";
        public const string Error = "alert-danger";
    }
}
