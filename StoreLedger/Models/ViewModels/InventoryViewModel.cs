﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace StoreLedger.Models.ViewModels
{
    public class InventoryViewModel
    {
        public Guid InventoryId { get; set; }

        [Required]
        public int Code { get; set; }

        
        public string ImageUrl { get; set; }

        public IFormFile ImageFile { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public string ItemType { get; set; }

        [Required]
        public int MinLevel { get; set; }

        [Required]
        public int MaxLevel { get; set; }

        [Required]
        public bool IsEnabled { get; set; } = true;

        [Required]
        public DateTime DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [Required]
        public int InventoryCategoryId { get; set; }

        [Required]
        public int ReOrderLevel { get; set; }
    }

    public class InventoryStockViewModel
    {
        public Guid InventoryStockId { get; set; }

        [Required]
        public Guid InventoryId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public decimal Price { get; set; }

        public int Code { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }

        public string Category { get; set; }

        public int? Sold { get; set; }
        public int? PrevQuantity { get; set; }
        public decimal? PrevPrice { get; set; }
        public int? PrevSold { get; set; }
    }

    public class SaleItemViewModel
    {
        public Guid InventoryId { get; set; }
        public int Quantity { get; set; }
        public int Code { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public decimal Total { get; set; }
        public Guid InventoryStockId { get; set; }
    }

    public class SaleViewModel
    {
        public string Items { get; set; }
        public decimal AmountPaid { get; set; }
    }

    public class TransactionViewModel
    {
        public List<SaleItemViewModel> Items { get; set; }
        public decimal AmountPaid { get; set; }
        public decimal AmountChange { get; set; }
        public decimal AmountCharged { get; set; }
    }

    public class ObsoleteStockViewModel
    {
        public Guid ObsoleteStockId { get; set; }

        [Required]
        public Guid InventoryId { get; set; }

        [Required]
        public Guid InventoryStockId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int ObsoleteQuantity { get; set; }

        [Required, Column(TypeName = "decimal(18, 6)")]
        public decimal Price { get; set; }

        [Required]
        public string ObsoleteType { get; set; }

        [Required]
        public string Notes { get; set; }

        
    }
}
