﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreLedger.Models.ViewModels
{
    public class DashboardViewModel
    {
        public DayViewModel Daily { get; set; }
        public DaysViewModel Weekly { get; set; }
        public DaysViewModel Montly { get; set; }
        public DaysViewModel Yearly { get; set; }
    }

    public class DayViewModel
    {
        public DateTime Date { get; set; }
        public decimal Total { get; set; }
    }

    public class DaysViewModel
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public decimal Total { get; set; }
    }
}
