﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace StoreLedger.Models.ViewModels
{
    public class LoginViewModel
    {
        [BindRequired]
        public string Username { get; set; }

        [BindRequired]
        public string Password { get; set; }
    }
}
