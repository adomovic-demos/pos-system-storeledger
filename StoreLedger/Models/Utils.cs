﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreLedger.Models
{
    public class Utils
    {

        public static string GetPrettyDate(DateTime d)
        {
            // 1.
            // Get time span elapsed since the date.
            TimeSpan s = DateTime.Now.Subtract(d);

            // 2.
            // Get total number of days elapsed.
            int dayDiff = (int)s.TotalDays;

            // 3.
            // Get total number of seconds elapsed.
            int secDiff = (int)s.TotalSeconds;

            // 4.
            // Don't allow out of range values.
            if (dayDiff < 0 || dayDiff >= 31)
            {
                return null;
            }

            // 5.
            // Handle same-day times.
            if (dayDiff == 0)
            {
                // A.
                // Less than one minute ago.
                if (secDiff < 60)
                {
                    return "just now";
                }
                // B.
                // Less than 2 minutes ago.
                if (secDiff < 120)
                {
                    return "1 minute ago";
                }
                // C.
                // Less than one hour ago.
                if (secDiff < 3600)
                {
                    return string.Format("{0} minutes ago",
                        Math.Floor((double)secDiff / 60));
                }
                // D.
                // Less than 2 hours ago.
                if (secDiff < 7200)
                {
                    return "1 hour ago";
                }
                // E.
                // Less than one day ago.
                if (secDiff < 86400)
                {
                    return string.Format("{0} hours ago",
                        Math.Floor((double)secDiff / 3600));
                }
            }
            // 6.
            // Handle previous days.
            if (dayDiff == 1)
            {
                return "yesterday";
            }
            if (dayDiff < 7)
            {
                return string.Format("{0} days ago",
                    dayDiff);
            }
            if (dayDiff < 31)
            {
                return string.Format("{0} weeks ago",
                    Math.Ceiling((double)dayDiff / 7));
            }
            return null;
        }

        public static string GetChartDateTitle(DateTime? from, DateTime? to)
        {
            var dateTitle = string.Empty;
            var format = "dd MMM, yyyy";

            if (from != null && to != null)
            {
                var dtFrom = Convert.ToDateTime(from);
                var dtTo = Convert.ToDateTime(to);

                if (dtFrom.Year != dtTo.Year)
                    dateTitle = "from " + dtFrom.ToString(format) + " to " + dtTo.ToString(format);
                else if (dtFrom.Month != dtTo.Month && dtFrom.Year == dtTo.Year)
                    dateTitle = "from " + dtFrom.ToString("dd MMM") + " to " + dtTo.ToString(format);
                else if (dtFrom.Month == dtTo.Month && dtFrom.Day != dtTo.Day && dtFrom.Year == dtTo.Year)
                    dateTitle = "from " + dtFrom.ToString("dd") + " to " + dtTo.ToString(format);
                else if (dtFrom == to)
                    dateTitle = "for " + dtTo.ToString(format);
            }
            else if (from != null)
                dateTitle = "for " + Convert.ToDateTime(from).ToString(format);
            else if (to != null)
                dateTitle = "for " + Convert.ToDateTime(to).ToString(format);

            return dateTitle;
        }

        public static string GetDashDateTitle(DateTime? from, DateTime? to)
        {
            var dateTitle = string.Empty;
            var format = "dd MMM, yyyy";

            if (from != null && to != null)
            {
                var dtFrom = Convert.ToDateTime(from);
                var dtTo = Convert.ToDateTime(to);

                if (dtFrom.Year != dtTo.Year)
                    dateTitle = $"<code>{dtFrom.ToString(format)}</code> to <code>{dtTo.ToString(format)}</code>";
                else if (dtFrom.Month != dtTo.Month && dtFrom.Year == dtTo.Year)
                    dateTitle = $"<code>{dtFrom:dd MMM}</code> to <code>{dtTo.ToString(format)}</code>";
                else if (dtFrom.Month == dtTo.Month && dtFrom.Day != dtTo.Day && dtFrom.Year == dtTo.Year)
                    dateTitle = $"<code>{dtFrom:dd}</code> to <code>{dtTo.ToString(format)}</code>";
                else if (dtFrom == to)
                    dateTitle = $"<code>{dtTo.ToString(format)}</code>";
            }
            else if (from != null)
                dateTitle = $"<code>{Convert.ToDateTime(@from).ToString(format)}</code>";
            else if (to != null)
                dateTitle = $"<code>{Convert.ToDateTime(to).ToString(format)}</code>";

            return dateTitle;
        }

        public static DateTime GetLastOccurenceOfDay(DateTime value, DayOfWeek weekStartDay)
        {
            var daysToAdd = weekStartDay - value.DayOfWeek;
            if (daysToAdd < 1)
            {
                daysToAdd -= 7;
            }
            return value.AddDays(daysToAdd);
        }

        public static DateTime GetFirstDayOfWeek(DateTime currDate)
        {
            var culture = System.Threading.Thread.CurrentThread.CurrentCulture;
            var diff = currDate.DayOfWeek - culture.DateTimeFormat.FirstDayOfWeek;
            if (diff < 0)
                diff += 7;

            return currDate.AddDays(-diff).Date;
        }

        public static DateTime GetLastDayOfWeek(DateTime currDate)
        {
            var wkStartDate = GetFirstDayOfWeek(currDate);
            return wkStartDate.AddDays(6);
        }
    }
}
