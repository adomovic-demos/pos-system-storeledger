﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;

namespace StoreLedger.Models
{
    public class Flash
    {

        public const string SessionKey = "flash";

        public static void FlashWarning(string message, HttpContext context)
        {
            var flashMessage = new Message
            {
                Type = MessageTypes.Warning,
                Body = message.Trim()
            };

            PersistMessage(flashMessage, context);
        }

        public static void FlashSuccess(string message, HttpContext context)
        {
            var flashMessage = new Message
            {
                Type = MessageTypes.Success,
                Body = message.Trim()
            };

            PersistMessage(flashMessage, context);
        }
        
        public static void FlashError(string message, HttpContext context)
        {
            var flashMessage = new Message
            {
                Type = MessageTypes.Error,
                Body = message.Trim()
            };

            PersistMessage(flashMessage, context);
        }

        private static void PersistMessage(Message flashMessage, HttpContext context)
        {
            context.Session.SetString(SessionKey, JsonConvert.SerializeObject(flashMessage));
        }
    }

    public class Message
    {
        public string Type { get; set; }
        public string Body { get; set; }
        public bool Dismissible { get; set; }
        public Message()
        {
            Dismissible = true;
        }
    }

    public class MessageTypes
    {
        public const string Success = "alert-success";
        public const string Warning = "alert-warning";
        public const string Error = "alert-danger";
    }

}
