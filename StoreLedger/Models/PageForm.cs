﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace StoreLedger.Models
{
    public class PageForm
    {
        public string Title { get; set; }
        public string Method { get; set; }

        public PageForm(string title, string method)
        {
            Title = title;
            Method = method;
        }
    }

    
}
