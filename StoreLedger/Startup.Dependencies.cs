﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using StoreLedger.Activities;
using StoreLedger.Model.Repos;

namespace StoreLedger
{
    public partial class Startup
    {
        public void DependencyInjects(IServiceCollection services)
        {
            //repos
            services.AddScoped<UserRoleRepo>();
            services.AddTransient<UserRepo>();
            services.AddTransient<InventoryCategoryRepo>();
            services.AddTransient<InventoryRepo>();
            services.AddTransient<InventoryStockRepo>();
            services.AddTransient<TransactionItemRepo>();
            services.AddTransient<TransactionRepo>();
            services.AddTransient<ObsoleteStockRepo>();

            //activities
            services.AddTransient<UserActivity>();
            services.AddTransient<InventoryActivity>();
            services.AddTransient<TransactionActivity>();
            services.AddTransient<LoginActivity>();
        }
    }
}
