﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreLedger.Model
{
    public class SeedData
    {
        public static string MasterRole =
            @"
            insert into UserRoles(Name) values('Super Administrator');
            insert into UserRoles(Name) values('Administrator');          
            insert into UserRoles(Name) values('Manager'); 
            insert into UserRoles(Name) values('Attendant');";

        public static string SuperAdmin = @"insert into Users(RoleId,FirstName,LastName,Username,Password,IsEnabled,MobileNumber)
                                            values(1,'Super','Administrator','Admin900','0055Root',1,'0244000')";

        public static string Admin = @"insert into Users(RoleId,FirstName,LastName,Username,Password,IsEnabled,MobileNumber)
                                            values(2,'Administrator','User','Admin001','passw0rd8',1,'0244000')";
    }

    public class SeedScripts
    {
        public static string FunctionGetStockSold = @"CREATE function [dbo].[GetStockSold](@Inventorystockid uniqueidentifier)
                                                    returns int
                                                    as
                                                    begin
	                                                    declare @Total int
	                                                    select @Total = sum(Quantity)
	                                                    from transactionitems
	                                                    where inventorystockid = @Inventorystockid

	                                                    if @Total  is null 
		                                                    set @Total = 0.0;
	
	                                                    return @Total;
                                                    end
                                                    GO";
    }
}
