﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StoreLedger.Model
{
    public class InventoryCategory
    {
        [Key]
        public int InventoryCategoryId { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
