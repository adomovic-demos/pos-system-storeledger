﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StoreLedger.Model
{
    public class TransactionItem
    {
        [Key]
        public Guid TransactionItemId { get; set; }

        [Required]
        public Guid TransactionId { get; set; }

        [Required]
        public Guid InventoryId { get; set; }

        [Required]
        public Guid InventoryStockId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public decimal Total { get; set; }

        public DateTime DateCreated { get; set; }
    }
}
