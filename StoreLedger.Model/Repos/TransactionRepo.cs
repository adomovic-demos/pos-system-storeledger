﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model.Repos
{
    public class TransactionRepo : GenericRepository<Transaction>
    {
        public TransactionRepo(StoreData context) : base(context)
        {
        }

        public async Task<List<TransactionAggregate>> GetAggregateReport(DateTime? from, DateTime? to)
        {
            var sql = @"select convert(date,datecreated) as DateCreated, 
                        sum(AmountCharged) as Total, 
                        avg(AmountCharged) as Average, 
                        min(AmountCharged) as Minimum, 
                        max(amountcharged) as Maximum 
                        from StoreLedger.dbo.Transactions 
                        where transactionid is not null";

            string sqlWhere;

            var parameters = new List<object>();

            if (from != null && to != null)
            {
                sqlWhere = @" and convert(date, datecreated) >= @from 
                              and convert(date, datecreated) <= @to";

                parameters.Add(new SqlParameter("from", from));
                parameters.Add(new SqlParameter("to", to));
            }
            else if (from != null)
            {
                sqlWhere = @" and convert(date, datecreated) >= @from";

                parameters.Add(new SqlParameter("from", from));
            }
            else if (to != null)
            {
                sqlWhere = @" and convert(date, datecreated) <= @to";

                parameters.Add(new SqlParameter("to", to));
            }
            else
            {
                var currDate = DateTime.Today;
                sqlWhere = @" and convert(date, datecreated) >= @from 
                              and convert(date, datecreated) <= @today";

                parameters.Add(new SqlParameter("from", currDate.AddDays(-30)));
                parameters.Add(new SqlParameter("today", currDate));
            }

            sql += sqlWhere;
            sql += @" group by convert(date,datecreated) 
                      order by convert(date,datecreated) desc";

            return await Context
                .Query<TransactionAggregate>()
                .FromSql(sql, parameters.ToArray())
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<TransactionTotal> GetTotal(DateTime date)
        {
            var sql = @"select isNull(sum(AmountCharged),0.0) as Total
                        from Transactions 
                        where convert(date, datecreated) = convert(date, @date)";

            var parameters = new List<object>
            {
                new SqlParameter("date", date)
            };
            
            return await Context
                .Query<TransactionTotal>()
                .FromSql(sql, parameters.ToArray())
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }

        public async Task<TransactionTotal> GetTotal(DateTime from, DateTime to)
        {
            var sql = @"select isNull(sum(AmountCharged),0.0) as Total
                        from Transactions 
                        where convert(date, datecreated) >= convert(date, @from) 
                        and convert(date, datecreated) <= convert(date, @to)";

            var parameters = new List<object>
            {
                new SqlParameter("from", from),
                new SqlParameter("to", to)
            };

            return await Context
                .Query<TransactionTotal>()
                .FromSql(sql, parameters.ToArray())
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }

        public async Task<List<TransactionTrend>> GetTrends(DateTime from, DateTime to)
        {
            var sql = @"select convert(date, datecreated) as DateCreated, isNull(sum(AmountCharged),0.0) as Total
                        from Transactions 
                        where convert(date, datecreated) >= convert(date, @from) 
                        and convert(date, datecreated) <= convert(date, @to)
                        group by convert(date, datecreated)
                        order by convert(date, datecreated) asc";

            var parameters = new List<object>
            {
                new SqlParameter("from", from),
                new SqlParameter("to", to)
            };

            return await Context
                .Query<TransactionTrend>()
                .FromSql(sql, parameters.ToArray())
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<List<TransactionTrendMonthly>> GetTrendsMonthly(DateTime from, DateTime to)
        {
            var sql = @"select datepart(year, datecreated) as Year, 
                        datepart(month, datecreated) as Month, 
                        DATENAME(month, datecreated) as MonthName,
                        isNull(sum(AmountCharged),0.0) as Total
                        from Transactions 
                        where convert(date, datecreated) >= convert(date, @from) 
                        and convert(date, datecreated) <= convert(date, @to)
                        group by datepart(year, datecreated), datepart(month, datecreated), DATENAME(month, datecreated)
                        order by datepart(year, datecreated), datepart(month, datecreated)";

            var parameters = new List<object>
            {
                new SqlParameter("from", from),
                new SqlParameter("to", to)
            };

            return await Context
                .Query<TransactionTrendMonthly>()
                .FromSql(sql, parameters.ToArray())
                .AsNoTracking()
                .ToListAsync();
        }



    }
}
