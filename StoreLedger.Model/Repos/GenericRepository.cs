﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model.Repos {
    public class GenericRepository<T> : IGenericRepository<T>
        where T : class
    {
        protected readonly StoreData Context;

        public GenericRepository(StoreData context)
        {
            Context = context;
        }

        public async Task<T> FindAsync(object id)
        {
            return await Context.Set<T>().FindAsync(id);
        }

        public async Task<List<T>> GetAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().Where(predicate).AsNoTracking().ToListAsync();
        }

        public async Task<List<T>> GetAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties)
        {

            IQueryable<T> query = Context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.Where(predicate).AsNoTracking().ToListAsync();
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, bool asNoTracking = true)
        {
            if(asNoTracking)
                return await Context.Set<T>().AsNoTracking().FirstOrDefaultAsync(predicate);

            return await Context.Set<T>().FirstOrDefaultAsync(predicate);
        }

        public async Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties)
        {

            IQueryable<T> query = Context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.AsNoTracking().FirstOrDefaultAsync(predicate);
        }

        public async Task<int> GetCountAsync(Expression<Func<T, bool>> predicate)
        {
            return await Context.Set<T>().Where(predicate).AsNoTracking().CountAsync();
        }

        public async Task<List<T>> GetAllAsync()
        {
            return await Context.Set<T>().AsNoTracking().ToListAsync();
        }

        public async Task<List<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties)
        {
            IQueryable<T> query = Context.Set<T>();
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.AsNoTracking().ToListAsync();
        }

        public async Task InsertAsync(T objectValue)
        {
            Context.Set<T>().Add(objectValue);
            await Context.SaveChangesAsync();
        }

        public async Task UpdateAsync(T objectValue)
        {
            Context.Entry(objectValue).State = EntityState.Modified;
            await Context.SaveChangesAsync();
        }

        public async Task DeleteAsync(object id)
        {
            var objectValue = await FindAsync(id);

            if (objectValue != null)
            {
                Context.Set<T>().Remove(objectValue);
                await Context.SaveChangesAsync();
            }
        }

        //todo: paginated results
    }

    public interface IGenericRepository<T> where T : class
    {
        Task<T> FindAsync(object id);

        Task<List<T>> GetAsync(Expression<Func<T, bool>> predicate);

        Task<List<T>> GetAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties);

        Task<List<T>> GetAllAsync();

        Task<List<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);

        Task InsertAsync(T objectValue);

        Task UpdateAsync(T objectValue);

        Task DeleteAsync(object id);

        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate, bool asNoTracking = true);

        Task<T> FirstOrDefaultAsync(Expression<Func<T, bool>> predicate,
            params Expression<Func<T, object>>[] includeProperties);

        Task<int> GetCountAsync(Expression<Func<T, bool>> predicate);
    }
}