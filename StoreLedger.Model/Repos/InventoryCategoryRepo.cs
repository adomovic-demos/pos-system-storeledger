﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model.Repos
{
    public class InventoryCategoryRepo : GenericRepository<InventoryCategory>
    {
        public InventoryCategoryRepo(StoreData context) : base(context)
        {
        }

        public async Task<List<InventoryCategory>> GetHavningInventory()
        {
            var sql = @"select distinct a.* 
                        from inventorycategories a inner join inventories b
                        on a.InventoryCategoryId = b.InventoryCategoryId";

            return await Context.InventoryCategories.FromSql(sql).ToListAsync();
        }
    }
}
