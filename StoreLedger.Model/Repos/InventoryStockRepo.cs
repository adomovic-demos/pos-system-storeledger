﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model.Repos
{
    public class InventoryStockRepo : GenericRepository<InventoryStock>
    {
        public InventoryStockRepo(StoreData context) : base(context)
        {
        }

        public async Task<InventoryStockView> GetWithSold(Guid inventoryId)
        {

            var sql = @"select *, dbo.GetStockSold(InventoryStockId) as Sold
                        from InventoryStocks
                        where InventoryId = @inventoryId and isActive = 1";

            return await Context
                .Query<InventoryStockView>()
                .FromSql(sql, new SqlParameter("inventoryId", inventoryId))
                .AsNoTracking()
                .FirstOrDefaultAsync();

        }
    }
}
