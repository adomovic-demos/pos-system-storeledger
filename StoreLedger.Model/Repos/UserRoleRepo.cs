﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreLedger.Model.Repos
{
    public class UserRoleRepo : GenericRepository<UserRole>
    {
        public UserRoleRepo(StoreData context) : base(context)
        {
            
        }
        
    }
}
