﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model.Repos
{
    public class TransactionItemRepo : GenericRepository<TransactionItem>
    {
        public TransactionItemRepo(StoreData context) : base(context)
        {
        }

        public async Task<List<TransactionItem>> GetDayReport(DateTime? date)
        {
            var sql = @"select *
                        from transactionitems
                        where TransactionItemId is not null ";

            string sqlWhere;

            SqlParameter parameter;

            if (date != null)
            {
                sqlWhere = @" and convert(date, datecreated) = @date";

                parameter = new SqlParameter("date", date);
            }
            else
            {
                sqlWhere = @" and convert(date, datecreated) = @today";

                parameter = new SqlParameter("today", DateTime.Today);
            }

            sql += sqlWhere;
            sql += " order by DateCreated desc";

            return await Context.TransactionItems.FromSql(sql, parameter).ToListAsync();

        }
    }
}
