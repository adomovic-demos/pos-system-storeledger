﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model.Repos
{
    public class InventoryRepo : GenericRepository<Inventory>
    {
        public InventoryRepo(StoreData context) : base(context)
        {
        }

        public async Task<List<InventoryView>> GetWithStock()
        {
            var sql = @"select a.*,b.Quantity, b.Price, c.Name as CategoryName,b.InventoryStockId, 
                        dbo.GetStockSold(b.InventoryStockId) as Sold
                        from Inventories a inner join InventoryStocks b
                        on a.InventoryId = b.InventoryId inner join InventoryCategories c
                        on a.InventoryCategoryId = c.InventoryCategoryId
                        where a.InventoryId = b.InventoryId and 
                        b.isActive = 1";

            return await Context.Query<InventoryView>()
                .FromSql(sql)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<StockSoldView> GetStockSold(Guid inventoryStockId)
        {
            var sql = @"select dbo.GetStockSold(@inventoryStockId) as Sold";

            var parameters = new List<object>
            {
                new SqlParameter("inventoryStockId", inventoryStockId)
            };

            return await Context.Query<StockSoldView>()
                .FromSql(sql, parameters.ToArray())
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }

        public async Task<List<InventoryView>> GetWithStockForSales()
        {
            var sql = @"select a.*,b.Quantity, b.Price, c.Name as CategoryName,b.InventoryStockId, 
                        dbo.GetStockSold(b.InventoryStockId) as Sold
                        from Inventories a inner join InventoryStocks b
                        on a.InventoryId = b.InventoryId inner join InventoryCategories c
                        on a.InventoryCategoryId = c.InventoryCategoryId
                        where a.InventoryId = b.InventoryId and 
                        b.isActive = 1 and b.Quantity > dbo.GetStockSold(b.InventoryStockId)";

            return await Context.Query<InventoryView>()
                .FromSql(sql)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
