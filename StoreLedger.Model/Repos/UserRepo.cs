﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreLedger.Model.Repos
{
    public class UserRepo : GenericRepository<User>
    {
        public UserRepo(StoreData context) : base(context)
        {
        }
    }
}
