﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace StoreLedger.Model
{
    public class ObsoleteStock
    {
        [Key]
        public Guid ObsoleteStockId { get; set; }

        [Required]
        public Guid InventoryId { get; set; }

        [Required]
        public Guid InventoryStockId { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required, Column(TypeName = "decimal(18, 6)")]
        public decimal Price { get; set; }

        [Required]
        public string ObsoleteType { get; set; }

        [Required]
        public string Notes { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }
    }
}
