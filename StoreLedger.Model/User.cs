﻿using System.ComponentModel.DataAnnotations;

namespace StoreLedger.Model
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        [Required]
        public int RoleId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Username { get; set; }
        [Required]
        public string Password { get; set; }
        [Required]
        public bool IsEnabled { get; set; }

        [Required]
        public string MobileNumber { get; set; }

        public UserRole UserRole { get; set; }
    }
}
