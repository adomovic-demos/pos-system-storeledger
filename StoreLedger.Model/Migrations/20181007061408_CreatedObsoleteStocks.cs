﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace StoreLedger.Model.Migrations
{
    public partial class CreatedObsoleteStocks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ObsoleteStocks",
                columns: table => new
                {
                    ObsoleteStockId = table.Column<Guid>(nullable: false),
                    InventoryId = table.Column<Guid>(nullable: false),
                    InventoryStockId = table.Column<Guid>(nullable: false),
                    Quantity = table.Column<int>(nullable: false),
                    Price = table.Column<decimal>(type: "decimal(18, 6)", nullable: false),
                    ObsoleteType = table.Column<string>(nullable: false),
                    Notes = table.Column<string>(nullable: false, type:"ntext"),
                    DateCreated = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ObsoleteStocks", x => x.ObsoleteStockId);
                });

            migrationBuilder.Sql(SeedScripts.FunctionGetStockSold);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ObsoleteStocks");
        }
    }
}
