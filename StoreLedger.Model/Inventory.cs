﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreLedger.Model
{
    public class Inventory
    {
        [Key]
        public Guid InventoryId { get; set; }

        [Required]
        public int Code { get; set; }

        [Required]
        public int UserId { get; set; }

        [Required]
        public string ImageUrl { get; set; }

        [Required]
        public string Name { get; set; }
        
        [Required]
        public string ItemType { get; set; }

        [Required]
        public int MinLevel { get; set; }

        [Required]
        public int MaxLevel { get; set; }

        [Required]
        public int ReOrderLevel { get; set; }

        [Required]
        public bool IsEnabled { get; set; } = true;

        [Required]
        public DateTime DateCreated { get; set; }

        public DateTime? DateModified { get; set; }

        [Required]
        public int InventoryCategoryId { get; set; }
        

        public ICollection<InventoryStock> Stocks { get; set; }
        public InventoryCategory Category { get; set; }

    }

    public class InventoryView
    {
        public Guid InventoryId { get; set; }

        public int Code { get; set; }

        public string ImageUrl { get; set; }

        public string Name { get; set; }

        public string ItemType { get; set; }

        public int MinLevel { get; set; }

        public int MaxLevel { get; set; }

        public int ReOrderLevel { get; set; }

        public int InventoryCategoryId { get; set; }

        public string CategoryName { get; set; }

        public int Quantity { get; set; }

        [Column(TypeName = "decimal(18, 2)")]
        public decimal Price { get; set; }

        public int Sold { get; set; }

        public Guid InventoryStockId { get; set; }
    }

    public class StockSoldView
    {
        public int Sold { get; set; }
    }
}
