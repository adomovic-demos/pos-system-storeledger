﻿using System;
using System.Collections.Generic;
using System.Text;

namespace StoreLedger.Model
{
    public class Defaults
    {
        public class Role
        {
            public const string SuperAdministrator = "Super Administrator";
            public const string Administrator = "Administrator";
            public const string Manager = "Manager";
            public const string Attendant = "Attendant";
            public const int SuperAdminId = 1;

            public const string AllAdmin = "Super Administrator,Administrator,Manager";
        }

        public class ItemTypeOption
        {
            public static string Single = "Single";
            public static string Pack = "Pack";
        }

        public enum PaymentMethod
        {
            Cash,
            MtnMomo,
            TigoMomo,
            VodafoneMomo,
            AirtelMomo,
            Hubtel,
            Slydepay,
            Expresspay
        }

        public class CountryProfile
        {
            public static string Currency = "GHS";
        }

        public class ObsoleteType
        {
            public static string Expired = "Expired";
            public static string Damaged = "Damaged";
            public static string FactoryDefect = "FactoryDefect";
        }

        public class RoleType
        {
            public static string FullName = "FullName";
            public static string RoleName = "RoleName";
        }

        public class UserType
        {
            public static string Id = "Id";
        }

    }
}
