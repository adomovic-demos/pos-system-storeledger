﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace StoreLedger.Model
{
    public class Transaction
    {
        [Key]
        public Guid TransactionId { get; set; }

        [Required]
        public decimal AmountCharged { get; set; }

        [Required]
        public decimal AmountPaid { get; set; }

        [Required]
        public decimal AmountChange { get; set; }

        public Guid CustomerId { get; set; } = new Guid();

        [Required]
        public DateTime DateCreated { get; set; }

        [Required]
        public int Userid { get; set; }

        [Required]
        public string PaymentMethod { get; set; }
    }

    public class TransactionAggregate
    {
        public decimal Total { get; set; }
        public decimal Average { get; set; }
        public decimal Minimum { get; set; }
        public decimal Maximum { get; set; }

        public DateTime DateCreated { get; set; }
        
    }

    public class TransactionTotal
    {
        public decimal Total { get; set; }

    }

    public class TransactionTrend
    {
        public DateTime DateCreated { get; set; }
        public decimal Total { get; set; }

    }

    public class TransactionTrendMonthly
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public string MonthName { get; set; }
        public decimal Total { get; set; }

    }
}
