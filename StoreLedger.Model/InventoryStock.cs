﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace StoreLedger.Model
{
    public class InventoryStock
    {
        [Key]
        public Guid InventoryStockId { get; set; }

        [Required]
        public Guid InventoryId { get; set; }
        
        [Required]
        public int Quantity { get; set; }

        [Required, Column(TypeName = "decimal(18, 6)")]
        public decimal Price { get; set; }

        [Required]
        public bool IsActive { get; set; }

        [Required]
        public DateTime DateCreated { get; set; }

    }

    public class InventoryStockView
    {
        public Guid InventoryStockId { get; set; }
        public Guid InventoryId { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public bool IsActive { get; set; }
        public DateTime DateCreated { get; set; }
        public int Sold { get; set; }
    }
}
