﻿using Microsoft.EntityFrameworkCore;

namespace StoreLedger.Model
{
    public class StoreData : DbContext
    {
        public StoreData(DbContextOptions<StoreData> options) : base(options)
        {
            
        }

        public DbSet<Inventory> Inventories { get; set; }
        public DbSet<InventoryStock> InventoryStocks { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserRole> UserRoles { get; set; }
        public DbSet<InventoryCategory> InventoryCategories { get; set; }
        public DbSet<TransactionItem> TransactionItems { get; set; }
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<ObsoleteStock> ObsoleteStocks { get; set; }

        public DbQuery<InventoryView> InventoryViews { get; set; }
        public DbQuery<InventoryStockView> InventoryStockViews { get; set; }
        public DbQuery<TransactionAggregate> TransactionAggregates { get; set; }
        public DbQuery<TransactionTotal> TransactionTotals { get; set; }
        public DbQuery<TransactionTrend> TransactionTrends { get; set; }
        public DbQuery<TransactionTrendMonthly> TransactionTrendYearlys { get; set; }
        public DbQuery<StockSoldView> StockSoldViews { get; set; }

    }
}
