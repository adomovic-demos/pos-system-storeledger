﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace StoreLedger.Model
{
    public class UserRole
    {
        [Key]
        public int RoleId { get; set; }
        [Required]
        public string Name { get; set; }

        public ICollection<User> Users { get; set; }
    }
}
